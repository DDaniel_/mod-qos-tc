#if !defined(MOD_TC_QOS_CONFIGURE_TBF_H__)
#define MOD_TC_QOS_CONFIGURE_TBF_H__

#ifdef __cplusplus
extern "C"
{
#endif
#include <amxc/amxc.h>
#include <qosnode/qos-node-api.h>

int activate_tbf_scheduler(qos_node_t* scheduler);

#ifdef __cplusplus
}
#endif

#endif // MOD_TC_QOS_CONFIGURE_TBF_H__
