#if !defined(MOD_TC_QOS_CONFIGURE_DRR_H__)
#define MOD_TC_QOS_CONFIGURE_DRR_H__

#ifdef __cplusplus
extern "C"
{
#endif
#include <amxc/amxc.h>
#include <qosnode/qos-node-api.h>

int activate_drr_scheduler(qos_node_t* scheduler);
int activate_drr_queue(qos_node_t* queue, uint32_t parentid);

#ifdef __cplusplus
}
#endif

#endif // MOD_TC_QOS_CONFIGURE_DRR_H__
