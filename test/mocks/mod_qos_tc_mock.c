/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc_macros.h>

#include "mod_qos_tc_mock.h"
struct nl_sock {
    int dummy;
};

struct rtnl_qdisc {
    int dummy;
};
struct rtnl_tc {
    int dummy;
};

struct rtnl_link {
    int dummy;
};
struct rtnl_cls {
    int dummy;
};

struct rtnl_class {
    int dummy;
};

struct nl_sock* __wrap_nl_socket_alloc() {
    int param = mock_type(int);
    if(param >= 0) {
        return (struct nl_sock*) calloc(1, sizeof(struct nl_sock));
    }
    return NULL;
}

struct rtnl_class* __wrap_rtnl_class_get(UNUSED struct nl_cache* cache, UNUSED int ifindex, UNUSED uint32_t handle) {
    int param = mock_type(int);
    if(param >= 0) {
        return (struct rtnl_class*) calloc(1, sizeof(struct rtnl_class));
    }
    return NULL;
}

void __wrap_rtnl_tc_set_link(UNUSED struct rtnl_tc* tc, UNUSED struct rtnl_link* link) {
}
void __wrap_rtnl_tc_set_handle(UNUSED struct rtnl_tc* tc, UNUSED uint32_t handle) {
}

void __wrap_rtnl_tc_set_parent(UNUSED struct rtnl_tc* tc, UNUSED uint32_t parent) {
}
void __wrap_rtnl_cls_set_protocol(UNUSED struct rtnl_cls* class, UNUSED uint16_t protocol) {
}
void __wrap_rtnl_cls_set_prio(UNUSED struct rtnl_cls* class, UNUSED uint16_t prio) {
}
int __wrap_rtnl_fw_set_classid(UNUSED struct rtnl_cls* class, UNUSED uint32_t classid) {
    return mock_type(int);
}
int __wrap_rtnl_fw_set_mask(UNUSED struct rtnl_cls* class, UNUSED uint32_t mask) {
    return mock_type(int);
}
int __wrap_rtnl_cls_add(UNUSED struct nl_sock* sock, UNUSED struct rtnl_cls* class, UNUSED int id) {
    return 0;
}
int __wrap_rtnl_tc_set_kind(UNUSED struct rtnl_tc* tc, UNUSED const char* kind) {
    return mock_type(int);
}
int __wrap_rtnl_cls_delete(UNUSED struct nl_sock* sock, UNUSED struct rtnl_cls* cls, UNUSED int id) {
    return 0;
}
void __wrap_rtnl_class_put(struct rtnl_class* class) {
    free(class);
}
void __wrap_rtnl_cls_put(struct rtnl_cls* cls) {
    free(cls);
}
void __wrap_nl_cache_put(UNUSED struct nl_cache* cache) {
}
void __wrap_rtnl_link_put(struct rtnl_link* link) {
    free(link);
}
void __wrap_nl_socket_free(UNUSED struct nl_sock* sock) {
    free(sock);
}
int __wrap_nl_connect(UNUSED struct nl_sock* sock, UNUSED int id) {
    return mock_type(int);
}
int __wrap_rtnl_link_alloc_cache(UNUSED struct nl_sock* sock, UNUSED int id, UNUSED struct nl_cache** cache) {
    return mock_type(int);
}
struct rtnl_link* __wrap_rtnl_link_get_by_name(UNUSED struct nl_cache* cache, UNUSED const char* name) {
    int param = mock_type(int);
    if(param >= 0) {
        return (struct rtnl_link*) calloc(1, sizeof(struct rtnl_link));
    }
    return NULL;
}
struct rtnl_qdisc* __wrap_rtnl_qdisc_alloc(void) {
    int param = mock_type(int);
    if(param >= 0) {
        return (struct rtnl_qdisc*) calloc(1, sizeof(struct rtnl_qdisc));
    }
    return NULL;
}
struct rtnl_class* __wrap_rtnl_class_alloc(void) {
    int param = mock_type(int);
    if(param >= 0) {
        return (struct rtnl_class*) calloc(1, sizeof(struct rtnl_class));
    }
    return NULL;
}
struct rtnl_cls* __wrap_rtnl_cls_alloc(void) {
    int param = mock_type(int);
    if(param >= 0) {
        return (struct rtnl_cls*) calloc(1, sizeof(struct rtnl_cls));
    }
    return NULL;
}
void __wrap_rtnl_qdisc_put(struct rtnl_qdisc* qdisc) {
    free(qdisc);
}
int __wrap_rtnl_class_delete(UNUSED struct nl_sock* sock, UNUSED struct rtnl_class* class) {
    return mock_type(int);
}
int __wrap_rtnl_qdisc_delete(UNUSED struct nl_sock* sock, UNUSED struct rtnl_qdisc* qdisc) {
    return mock_type(int);
}
int __wrap_rtnl_htb_set_defcls(UNUSED struct rtnl_qdisc* qdisc, UNUSED uint32_t cls) {
    return mock_type(int);
}
int __wrap_rtnl_qdisc_add(UNUSED struct nl_sock* sock, UNUSED struct rtnl_qdisc* qdisc, UNUSED int id) {
    return mock_type(int);
}
int __wrap_rtnl_htb_set_rate(struct rtnl_class* class, uint32_t rate) {
    fprintf(stderr, "htb_set_rate: %p, %u\n", class, rate);
    check_expected(class);
    check_expected(rate);
    return mock_type(int);
}
int __wrap_rtnl_htb_set_ceil(struct rtnl_class* class, uint32_t ceil) {
    check_expected(class);
    check_expected(ceil);
    return mock_type(int);
}
int __wrap_rtnl_htb_set_cbuffer(UNUSED struct rtnl_class* class, UNUSED uint32_t cbuffer) {
    return mock_type(int);
}
int __wrap_rtnl_class_add(UNUSED struct nl_sock* sock, UNUSED struct rtnl_class* class, UNUSED int id) {
    return mock_type(int);
}
int __wrap_rtnl_htb_set_prio(UNUSED struct rtnl_class* class, UNUSED uint32_t prio) {
    return mock_type(int);
}
void __wrap_rtnl_sfq_set_perturb(UNUSED struct rtnl_qdisc* qdisc, UNUSED uint32_t perturb) {
}
void __wrap_rtnl_red_set_limit(UNUSED struct rtnl_qdisc* qdisc, UNUSED int limit) {
}
uint64_t __wrap_rtnl_tc_get_stat(UNUSED struct rtnl_class* class, UNUSED enum rtnl_tc_stat id) {
    return mock_type(int);
}
int __wrap_rtnl_class_alloc_cache(UNUSED struct nl_sock* socket, UNUSED int ifindex, UNUSED struct nl_cache* cache) {
    return mock_type(int);
}
int __wrap_rtnl_link_get_ifindex(UNUSED struct rtnl_link* link) {
    return mock_type(int);
}
const char* __wrap_qos_node_queue_get_interface(UNUSED qos_node_t* node) {
    return mock_type(const char*);
}
uint32_t __wrap_qos_node_queue_get_index(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
uint32_t __wrap_qos_node_queue_get_queue_key(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
uint32_t __wrap_qos_node_queue_get_precedence(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
const char* __wrap_qos_node_scheduler_get_interface(UNUSED qos_node_t* node) {
    return mock_type(const char*);
}
const char* __wrap_qos_node_shaper_get_interface(UNUSED qos_node_t* node) {
    return mock_type(const char*);
}
uint32_t __wrap_qos_node_shaper_get_shaping_rate(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
uint32_t __wrap_qos_node_shaper_get_shaping_burst_size(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
uint32_t __wrap_qos_node_shaper_get_index(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
int32_t __wrap_qos_node_queue_get_shaping_rate(UNUSED qos_node_t* node) {
    return mock_type(int32_t);
}
int32_t __wrap_qos_node_queue_get_assured_rate(UNUSED qos_node_t* node) {
    return mock_type(int32_t);
}
uint32_t __wrap_qos_node_queue_get_shaping_burst_size(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
qos_scheduler_algorithm_t __wrap_qos_node_queue_get_scheduler_algorithm(UNUSED qos_node_t* node) {
    return mock_type(qos_scheduler_algorithm_t);
}
uint32_t __wrap_qos_node_queue_get_red_threshold(UNUSED qos_node_t* node) {
    return mock_type(uint32_t);
}
qos_queue_drop_algorithm_t __wrap_qos_node_queue_get_drop_algorithm(UNUSED qos_node_t* node) {
    return mock_type(qos_queue_drop_algorithm_t);
}
qos_scheduler_algorithm_t __wrap_qos_node_scheduler_get_scheduler_algorithm(UNUSED qos_node_t* node) {
    return mock_type(qos_scheduler_algorithm_t);
}
qos_node_t* __wrap_qos_node_scheduler_get_default_queue_node(UNUSED qos_node_t* node) {
    return mock_type(qos_node_t*);
}
qos_node_t* __wrap_qos_node_find_parent_by_type(UNUSED const qos_node_t* node, UNUSED qos_node_type_t type) {
    return mock_type(qos_node_t*);
}
qos_node_t* __wrap_qos_node_get_node(UNUSED const char* path) {
    return mock_type(qos_node_t*);
}
