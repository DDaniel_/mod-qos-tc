# mod_qos-tc

[TOC]

# Introduction

To know the position of this modue in the overall QoS subsystem, the following
image provides some clarification.

![Overview](doc/images/mod_qos-tc_overview.svg)

From top to bottom, these components can be seen:

- The TR-181 Device.QoS data model.
- tr181-qos: the userspace application, taking care of the data model
  configuration.
- libqoscommon: provides general functions for tr181-qos and libqosnode.
- libqosnode: an additional layer, abstracting the TR-181 data model and used
  to interface with the QoS modules.
- mod_qos-tc: this module. It's main task is to convert TR-181 data model
  parameters to the API provided by linux tc.

# Trace zone

Add trace zone "qos-tc" to see traces in the log files.

# TR-181 data model & examples

Consult the full documentation.

# Building, installing and testing

## Docker container

You could install all tools needed for testing and developing on your local
machine, or use a pre-configured environment. Such an environment is already
prepared for you as a docker container.

1. Install Docker

    Docker must be installed on your system. Here are some links that could
    help you:

    - [Get Docker Engine - Community for
      Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for
      Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for
      Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for
      CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
      <br /><br />

    Make sure you user id is added to the docker group:

        sudo usermod -aG docker $USER
    <br />

2. Fetch the container image

    To get access to the pre-configured environment, pull the image and launch
    a container.

    Pull the image:

        docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    Before launching the container, you should create a directory which will be
    shared between your local machine and the container.

        mkdir -p ~/amx/qos/modules/

    Launch the container:

        docker run -ti -d \
            --name oss-dbg \
            --restart=always \
            --cap-add=SYS_PTRACE \
            --sysctl net.ipv6.conf.all.disable_ipv6=1 \
            -e "USER=$USER" \
            -e "UID=$(id -u)" \
            -e "GID=$(id -g)" \
            -v ~/amx:/home/$USER/amx \
            registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    The `-v` option bind mounts the local directory for the amx project in
    the container, at the exact same place.
    The `-e` options create environment variables in the container. These
    variables are used to create a user name with exactly the same user id and
    group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

        docker exec -ti --user $USER oss-dbg /bin/bash

## Building

### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc) - Generic C api for common data containers
- [libamxm](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm) - Common patterns implementations
- [libqosnode](https://gitlab.com/prpl-foundation/components/core/libraries/libqosnode) - The QoS node api
- [libnl](https://github.com/tgraf/libnl) - Libraries providing APIs to netlink protocol based Linux kernel interfaces
- [libsahtrace](https://gitlab.com/prpl-foundation/components/core/libraries/libsahtrace) - Small and flexible library to enable tracing and logging

### Runtime dependencies
- [kmod-sched]
- [kmod-sched-core]
- [libnl-route]

### Build mod_qos-tc

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    just created for the ambiorix project and clone this library in it (on your
    local machine).

        cd ~/amx/qos
        mkdir modules
        cd modules
        git clone git@gitlab.com:prpl-foundation/components/core/modules/mod-qos-tc.git

2. Build it

    When using the internal gitlab, you must define an environment variable
    `VERSION_PREFIX` before building in your docker container.

        export VERSION_PREFIX="master_"

    After the variable is set, you can build the package.

        cd ~/amx/qos/modules/mod_qos-tc
        make

## Installing

### Using make target install

You can install your own compiled version easily in the container by running
the install target.sudo apt-get install mod-sahtrace sah-lib-sahtrace-dev iproute2

    cd ~/amx/qos/qos/modules/mod_qos-tc
    sudo -E make install

### Using package

From within the container you can create packages.

    cd ~/amx/qos/qos/modules/mod_qos-tc
    make package

The packages generated are:

    ~/amx/qos/qos/modules/mod_qos-tc/mod-qos-tc-<VERSION>.tar.gz
    ~/amx/qos/qos/modules/mod_qos-tc/mod-qos-tc-<VERSION>.deb

You can copy these packages and extract/install them.

For Ubuntu or Debian distributions use dpkg:

    sudo dpkg -i ~/amx/qos/qos/modules/mod_qos-tc/mod-qos-tc-<VERSION>.deb

## Testing

### Prerequisites

No extra components are needed for testing `mod_qos-tc`.

### Run tests

You can run the tests by executing the following command:

    cd ~/amx/qos/qos/modules/mod_qos-tc/test
    make

Or this command if you also want the coverage tests to run:

    cd ~/amx/qos/qos/modules/mod_qos-tc/tests
    make run coverage

You can combine both commands:

    cd ~/amx/qos/qos/modules/mod_qos-tc
    make test

### Coverage reports

The coverage target will generate coverage reports using
[gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and
[gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each c-file is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are
available in the output directory of the compiler used.  For example, when using
native gcc, the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML
coverage reports can be found at
`~/amx/qos/qos/modules/mod_qos-tc/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser. In the container start a
python3 http server in background.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/modules/mod_qos-tc/output/<MACHINE>/coverage/report

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/qos/modules/mod_qos-tc$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/qos/qos/modules/mod_qos-tc/output/x86_64-linux-gnu/coverage/report/`

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths

The documentation is split in two parts, starting from the repository path:

    cd ~/amx/qos/qos/modules/mod_qos-tc

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.

The code itself is documented in the approriate header and source files.
Documentation is only generated for the header files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/amx/qos/qos/modules/mod_qos-tc
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser. As described in the coverage reports
section, you can start a http server in the container.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/qos/modules/mod_qos-tc/output/html/index.html

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/qos/modules/mod_qos-tc$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/qos/qos/modules/mod_qos-tc/output/html/index.html`

# What's next?

Consult the full documentation for a complete description of the TR-181 data
model mapping, code examples, ...

