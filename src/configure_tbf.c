/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
//#include <qosnode/qos-node-api.h>

#include <netlink/route/tc.h>
#include <netlink/route/qdisc.h>
#include <netlink/route/qdisc/tbf.h>
//#include <netlink/utils.h>

#include "tc_qos.h"
#include "configure_tc.h"
#include "configure_tbf.h"

int activate_tbf_scheduler(qos_node_t* scheduler) {
    int retval = -2;
    netlink_return netlink_data;
    const char* ifname = qos_node_scheduler_get_interface(scheduler);
    uint32_t rate;
    uint32_t burst_size;
    qos_node_t* shaper = qos_node_find_child_by_type(scheduler, QOS_NODE_TYPE_SHAPER);

    when_null(shaper, exit);
    when_str_empty(ifname, exit);

    rate = qos_node_shaper_get_shaping_rate(shaper) / 8;
    burst_size = qos_node_shaper_get_shaping_burst_size(shaper);
    when_false(rate, misconfigured);
    when_false(burst_size, misconfigured);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    when_failed(retval, failed);

    rtnl_tc_set_link(TC_CAST(netlink_data.qdisc), netlink_data.link);
    rtnl_tc_set_parent(TC_CAST(netlink_data.qdisc), TC_H_ROOT);
    rtnl_tc_set_handle(TC_CAST(netlink_data.qdisc), TC_HANDLE(1, 0));
    retval = rtnl_tc_set_kind(TC_CAST(netlink_data.qdisc), "tbf");
    when_failed(retval, failed);

    rtnl_qdisc_tbf_set_limit(netlink_data.qdisc, rate);
    rtnl_qdisc_tbf_set_rate(netlink_data.qdisc, rate, burst_size, 8);

    retval = rtnl_qdisc_add(netlink_data.sock, netlink_data.qdisc, NLM_F_CREATE);
    when_failed(retval, failed);

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_QDISC);
exit:
    return retval;

failed:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_QDISC);
    return -1;

misconfigured:
    return -2;
}

