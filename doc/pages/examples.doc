/*!

@page examples Examples

@details This page describes example configurations for Linux Traffic Control, at the moment only HTB is supported. Check the
@ref tr181 page to see the required (or optional) parameters for a valid QoS
configuration.

@section example_htb Example HTB configuration
@subsection overview HTB hierarchy

The next image shows an example of a simple Linux TC configuration. Two networks are
defined here, home and guest. For the home network, different kinds of traffic
are prioritized using different queues, while the guest network only has one
queue for all traffic.

- Home network, traffic is divided in:
    - Regular internet data (queue-data).
    - Voice over IP (queue-voip).
    - IPTV (queue-iptv).

- Guest network
    - All traffic goes through the same low priority queue (queue-guest).

The home network queues first go through another queue (queue-home), before
arriving at the shaper. The shaper has a second input, the queue from the
guest network. A (HTB) scheduler, belonging to the wan interface, is connected to the
network interface.

@image html examples/example_qos_hierarchy_htb.svg "Simple HTB QoS configuration"

All blocks above are objects of Device.Qos.{Queue, QueueStats, Scheduler,
Shaper}, except for the Interface.

@subsection config Configuration
@include examples/example_htb.odl

Using the Linux Traffic Control `tc` command, the configuration can be verified.

@verbatim
USER@<CID>:~$ tc qdisc show dev eth0
qdisc htb 1: root refcnt 5 r2q 10 default 0x10003 direct_packets_stat 0 direct_qlen 1000   <-- scheduler-eth0

USER@<CID>:~$ tc class show dev eth0
class htb 1:32 root rate 325Mbit ceil 325Mbit burst 407671b cburst 407671b                 <-- shaper-eth0
class htb 1:1 parent 1:32 prio 5 rate 25Mbit ceil 25Mbit burst 32750b cburst 32750b        <-- queue-guest
class htb 1:2 parent 1:32 rate 300Mbit ceil 300Mbit burst 376500b cburst 376500b           <-- queue-home
class htb 1:3 parent 1:2 prio 3 rate 250Mbit ceil 250Mbit burst 314000b cburst 314000b     <-- queue-home-data
class htb 1:4 parent 1:2 prio 1 rate 10Mbit ceil 10Mbit burst 14000b cburst 14000b         <-- queue-home-voip
class htb 1:5 parent 1:2 prio 2 rate 40Mbit ceil 40Mbit burst 51495b cburst 51495b         <-- queue-home-iptv

@endverbatim







*/
